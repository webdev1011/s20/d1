const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const viewersRoutes = require('./routes/Viewer');
const port = 3000;
const app = express();

app.use(cors());

/*connect to mongodb atlas via mongoose*/
mongoose.connect('mongodb+srv://dbUser:dbUser@cluster0.loryv.mongodb.net/B100To_do?retryWrites=true&w=majority', {
	 useNewUrlParser: true,
	 useUnifiedTopology: true,
	 useFindAndModify: false
});


//set notification for connection success or failure
let db = mongoose.connection;

//if connection error encoountered, output it in console
db.on('error', (console.error.bind(console, 'connection error: ')));

//once connection, show notification
db.once('open', () => console.log("We're connected to our database."));

//error handling
//error tracing

/*Middleware - built -in function ng app natin*/
//express.json() - it has a features of codes that we can use. It tells our app that all request that we will get is a json format
app.use(express.json());

//"{name: 'eat', status: 'pending'}"
//if we remove the quotation, it is actually an object
//so how the server will know the specific data that we are getting. it has a parsing to analyze a string/text and converting it to an object to make it readable to our database.
//req.body


app.use(express.urlencoded( { extended:true }));

/*express {
	json: ()=>{}
}
*/


//define Schema - structure of documents
//When we want to create a Schema this is done via Schema() constructor of the mongoose module
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'pending'
	}
})
 

 const Task = mongoose.model('Task', taskSchema);
 //MVC = model(blueprints), views(user interface), controllers(logic)

/*taskSchema {
	save: ()=>
}
schema:  ()=>*/



 /* Create a user schema using mongoose with the following fields
	username: string
	email: string
	password: string
	//tasks is an array of embedded task documents, it refers to taskSchma
	tasks: array

  */
 
 const userSchema = new mongoose.Schema({
 	username: String,
	email: String,
	password: String,
	//tasks is an array of embedded task documents. it referes to taskSchema in this case.
	tasks: [taskSchema]
 })
 const User = mongoose.model('User', userSchema);


//register a new user
app.post('/users', (req, res)=> {
	//1. check if username or email are duplicate prior to registration

	//User == UserSchema
	//.find() mongoose method
	//conditional statement
/*	{ $or: performs a logical OR operator in an array
		[
		{ username: req.body.username }, 
		{ email: req.body.email }
		]
	}*/

/*
[{Judy}, {Janine}, {kristel}]


duplicate.length() > 0
0 > 0
Gab
*/
	User.find({ $or: [{ username: req.body.username }, { email: req.body.email }] }, (findErr, duplicates) => {
			//2. if error return
			if(findErr) return console.error(findErr);
			//3. if duplicates found returns:
			if(duplicates.length > 0) {
				return res.status(403).json({//forbidden
					message: "Duplicates found, kindly choose different username and/or email."
				})
				//4. if no duplicates found save the data
			} else {
			//5. instantiate a new user object with properties derived from the request body
			let newUser = new User({
				username: req.body.username,
				email: req.body.email,
				password: req.body.password,
				//tasks is initially an empty array
				tasks: []
			})

/*<body>
	<form>
		<input name="username"/> 
	</form>
</body>

newUser {
	save: ()=>
}
*/

			//6. save the new user
			newUser.save((saveErr, newUser) => {
				//if an error was ecncounter while saving the document - notification in the console
				if(saveErr) return console.error(saveErr);
				return res.status(201).json({
					//status 201, successful creation
					//status 200-299 success response
					//400-499 related client
					//500 and above error connecting to your server
						message: `User ${newUser.username} successfully registered,`,
						data: {
							username: newUser.username,
							email: newUser.email,
							link_to_self: `/user/${newUser._id}`
						}
				});
			})
		}
	})	
});



//display user
//1. routes
//2. find a specific user
//3. if error return error
//4. else return User retrieved successfully
		app.get('/users/:id', (req, res) => {
			User.findById(req.params.id, (err, user) => {
				if(err) return console.error(err);
				return res.status(200).json({
					message: "User retrieved successfully.",
					data: {
						username: user.username,
						email: user.email,
						tasks: `/users/${user._id}/tasks`//just returning the route or profile of the user
					}
				})
			})
		})

//Create a new task for a specific user

//1. let's create a route

	app.post('/users/:userId/tasks', (req, res)=>{
//2. find the user using the query findById
	User.findById(req.params.userId, (findErr, user)=>{
//3. If error, return error
		if(findErr) return console.error(findErr);		
//4. If the user currently has no task, add a new task and save
		if(user.tasks === []){//user.tasks.length === 0
			//add the new task as a subdocument to the tasks array
			user.tasks.push({
				name: req.body.name
			});
			user.save((saveErr, modifiedUser)=>{
				if(saveErr) return console.error(saveErr);
				return res.status(200).json({
					message: `${user.tasks[0].name} added to task list of ${user.username}`,
					data: modifiedUser.tasks[0]
				})
			});
		}else{
//5. else, look for duplicates, if may duplicate, return the task is already registered
		let dupes = user.tasks.filter(task => task.name.toLowerCase() === req.body.name.toLowerCase());

		if(dupes.length > 0) {
			return res.status(403).json({
				message: `${req.body.name} is already registered as a task.`
			})
		}
		else{
//6. else if no duplicates found, save the task
			user.tasks.push({
				name: req.body.name
			});
			user.save((saveErr, modifiedUser)=>{
				if(saveErr) return console.error(saveErr);
				return res.status(200).json({
					message: `Added new task successfully`,
					//sample: [{eat}, {sleep}, {code}]
					data: modifiedUser.tasks[modifiedUser.tasks.length-1]
				})
			});
		}
	}
	})
	})


//get details of a specific task of a user
//1. create routes
//2. find the specific user, if error, return the error
//3. access the specific task of a user if task is null, return cannot be found
//4. else return task wit id <taskId> found




/*Routes*/
app.use('/', viewersRoutes);



//create port - communication - 3000

app.listen(port, () => { console.log(`Listening on port ${port}.`)})
