const express = require('express');
const mongoose = require('mongoose')
const router = express.Router();

/* GET */
// router.get('/fetch', (request, response) => {
// 	response.send('List of Viewers.')
// })

// router.get('/get', (req, res) => {
// 	res.json({
// 		"name": "Juan",
// 		"gender": "male"
// 	});
// });

//request.query
// router.get('/fetch', (req, res) => {
// 	//console.log(req.query);
// 	res.send(req.query);
// });

/* POST*/
// router.post('/insert', (req, res) => {
// 	res.json({
// 		"message": "Success!"
// 	})
// })

// router.post('/post', (req, res) => {
// 	res.json({
// 		"name": "Brandon",
// 		"lastName": "Smith",
// 		"message": "You are already a member."
// 	});
// });

// router.post('/insert', (req, res) => {
// 	console.log(req.body);
// });

// //destructure

// router.get('/fetch', (req, res) => {
// 	const { name, age, gender} = req.query

// 	if(age === 24) {
// 		res.status(200).send({
// 			name, //"name": name
// 			age,
// 			gender
// 		})
// 	} else {
// 		res.status(500).send({
// 			"message": "Age is not equal.",
// 			"status": "Failed!"
// 		});
// 	}
// });

module.exports = router;